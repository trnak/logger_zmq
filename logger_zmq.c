//requires: libzmq3-dev
//cc -Wall -O3 -o logger_zmq logger_zmq.c bcm2835.c -lzmq -pthread -lrt
//usage: logger_zmq [--sw-pwm] [spi_in_endpoint [spi_out_endpoint]]
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <time.h>
#include <zmq.h>
#include "bcm2835.h"

#define SPI_CHECK 0x00
#define SPI_CRCM_OK 0x3F
#define SPI_DATA_READY_MIN 0x40
#define SPI_DATA_READY_MAX 0x7F
#define SPI_CRC_DEFAULT 0x5F
#define IQRF_TRANSFER_CMD 0xF0
#define COMMUNICATION_MODE 0x80
#define IQRF_NMAX 64

enum CTYPE {WRITE_TO, READ_FROM};

const uint8_t chip_select = BCM2835_SPI_CS0;
const uint64_t delay_between_bytes = 100;
char* binary_name = "logger_zmq";
char* spi_in_endpoint = "tcp://127.0.0.1:5557";
char* spi_out_endpoint = "tcp://127.0.0.1:5556";

int signal_caught;
int sw_pwm;
pthread_mutex_t bcm_mutex;

void signal_handler(int sig) {
    signal_caught = 1;
}

int64_t timespec_remaining(struct timespec* next_tick, struct timespec* current_time) {
    int64_t rem = 0;
    rem += next_tick->tv_sec;
    rem -= current_time->tv_sec;
    rem *= 1000000000L;
    rem += next_tick->tv_nsec;
    rem -= current_time->tv_nsec;
    return rem;
}

void* sw_pwm_fn(void *arg) {
    struct timespec pwm_period = {.tv_sec = 4, .tv_nsec = 0};
    struct timespec next_tick;
    struct timespec current_time;
    struct timespec sleep_time;
    int64_t rem_time;
    clock_gettime(CLOCK_MONOTONIC, &next_tick);

    while (!signal_caught) {
        clock_gettime(CLOCK_MONOTONIC, &current_time);
        rem_time = timespec_remaining(&next_tick, &current_time);
        if (rem_time > 100000000) {
            sleep_time.tv_sec = 0;
            sleep_time.tv_nsec = 1000000;
            if (rem_time > 150000000) {
                sleep_time.tv_nsec = 50000000;
            }
            nanosleep(&sleep_time, NULL);
            continue;
        }

        pthread_mutex_lock(&bcm_mutex);
        bcm2835_gpio_set(18);
        pthread_mutex_unlock(&bcm_mutex);

        do {
            sleep_time.tv_sec = 0;
            sleep_time.tv_nsec = 1000000;
            nanosleep(&sleep_time, NULL);

            clock_gettime(CLOCK_MONOTONIC, &current_time);
            rem_time = timespec_remaining(&next_tick, &current_time);
        } while (rem_time > 1000000);

        pthread_mutex_lock(&bcm_mutex);
        bcm2835_gpio_clr(18);
        pthread_mutex_unlock(&bcm_mutex);

        next_tick.tv_sec += pwm_period.tv_sec;
        next_tick.tv_nsec += pwm_period.tv_nsec;
    }

    return ((void*)0);
}


uint8_t spi_transfer_byte(uint8_t b) {
    pthread_mutex_lock(&bcm_mutex);
    bcm2835_delayMicroseconds(delay_between_bytes);
    uint8_t c = bcm2835_spi_transfer(b);
    pthread_mutex_unlock(&bcm_mutex);
    return c;
}

int spi_transfer_data(const uint8_t len, uint8_t* buf, enum CTYPE ctype) {
    if (len > IQRF_NMAX) {
        printf("Cannot transfer %d bytes over SPI.\n", len);
        return 0;
    }

    if (NULL == buf) {
        printf("Buffer pointer is NULL.\n");
        return 0;
    }

    uint8_t ptype = len;
    ptype |= (WRITE_TO == ctype) ? 0x80 : 0x00;

    uint8_t command = IQRF_TRANSFER_CMD;
    uint8_t crcm = SPI_CRC_DEFAULT ^ command;
    uint8_t crcs = SPI_CRC_DEFAULT;
    uint8_t status = spi_transfer_byte(command);

    switch (ctype) {
        case WRITE_TO:
            if (COMMUNICATION_MODE != status) {
                printf("Module is not in communication mode: 0x%02x\n", status);
                return 0;
            }
            break;
        case READ_FROM:
            if ((SPI_DATA_READY_MIN > status) || (status > SPI_DATA_READY_MAX)) {
                printf("Module has no data ready: 0x%02x\n", status);
                return 0;
            }
            if (((SPI_DATA_READY_MIN == status) ? IQRF_NMAX : (status - SPI_DATA_READY_MIN)) != len) {
                printf("Module has data ready for transfer, but the requested length %d does not match: 0x%02x\n", len, status);
                return 0;
            }
            break;
    }

    uint8_t status2 = spi_transfer_byte(ptype);
    if (status != status2) {
        printf("Module status mismatch: 0x%02x != 0x%02x\n", status, status2);
        return 0;
    }

    crcm ^= ptype;
    crcs ^= ptype;

    uint8_t i;
    for (i = 0; i < len; i += 1) {
        uint8_t tx = (WRITE_TO == ctype) ? buf[i] : 0x00;
        crcm ^= tx;
        uint8_t rx = spi_transfer_byte(tx);
        crcs ^= rx;
        if (READ_FROM == ctype) {
            buf[i] = rx;
        }
        //printf("%02x %02x\n", tx, rx);
    }
    if (crcs != spi_transfer_byte(crcm)) {
        printf("CRCS mismatch, expected: 0x%02x\n", crcs);
        return 0;
    }
    if (SPI_CRCM_OK != spi_transfer_byte(SPI_CHECK)) {
        printf("CRCM mismatch.\n");
        return 0;
    }
    return 1;
}

int main(int argc, char* argv[]) {
    printf("$Build: %s %s$\n", __DATE__, __TIME__);
    signal_caught = 0;
    sw_pwm = 0;

    if (argc >= 1) {
        binary_name = argv[0];
    }
    if (argc >= 2) {
        if (strcmp("--sw-pwm", argv[1]) == 0) {
            sw_pwm = 1;
            argc -= 1;
            argv += 1;
        } else if (strcmp("--hw-pwm", argv[1]) == 0) {
            sw_pwm = 0;
            argc -= 1;
            argv += 1;
        }
    }
    if (argc >= 2) {
        spi_in_endpoint = argv[1];
    }
    if (argc >= 3) {
        spi_out_endpoint = argv[2];
    }

    struct sigaction act;
    memset(&act, 0, sizeof(act));
    sigemptyset(&act.sa_mask);
    act.sa_handler = signal_handler;
    act.sa_flags = 0;
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGINT, &act, NULL);

    void* ctx = zmq_ctx_new();
    void* rx = zmq_socket(ctx, ZMQ_SUB);
    zmq_setsockopt(rx, ZMQ_SUBSCRIBE, "", 0);
    printf("spi_in_endpoint: %s\n", spi_in_endpoint);
    if (0 != zmq_bind(rx, spi_in_endpoint)) {
        printf("zmq_bind() failed!\n");
        return EXIT_FAILURE;
    }
    void* tx = zmq_socket(ctx, ZMQ_PUB);
    int zmq_linger_period = 0;
    zmq_setsockopt(tx, ZMQ_LINGER, &zmq_linger_period, sizeof(zmq_linger_period));
    printf("spi_out_endpoint: %s\n", spi_out_endpoint);
    if (0 != zmq_bind(tx, spi_out_endpoint)) {
        printf("zmq_bind() failed!\n");
        return EXIT_FAILURE;
    }

    printf("bcm2835_init()\n");
    if (0 == bcm2835_init()) {
        printf("bcm2835_init() failed!\n");
        return EXIT_FAILURE;
    }
    if (0 == bcm2835_spi_begin()) {
        printf("bcm2835_spi_begin() failed!\n");
        return EXIT_FAILURE;
    }

    if (0 != pthread_mutex_init(&bcm_mutex, NULL)) {
        printf("mutex_init() failed\n");
        return EXIT_FAILURE;
    }

    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_1024);
    bcm2835_spi_chipSelect(chip_select);
    bcm2835_spi_setChipSelectPolarity(chip_select, LOW);

    pthread_t pwm_thread;
    if (sw_pwm) {
        printf("start sw pwm\n");
        bcm2835_gpio_fsel(18, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_clr(18);
        if (0 != pthread_create(&pwm_thread, NULL, sw_pwm_fn, NULL)) {
            printf("pthread_create() failed\n");
            return EXIT_FAILURE;
        }
    } else {
        printf("start hw pwm\n");
        bcm2835_gpio_fsel(18, BCM2835_GPIO_FSEL_ALT5);
        bcm2835_pwm_set_clock(192);
        bcm2835_pwm_set_range(0, 400000);
        bcm2835_pwm_set_data(0, 10000);
        bcm2835_pwm_set_mode(0, 1, 1);
    }

    uint8_t buf_spi_in[IQRF_NMAX];
    uint8_t buf_spi_out[IQRF_NMAX];
    int n, r;
    printf("enter transfer loop\n");
    while (!signal_caught) {
        uint8_t status = spi_transfer_byte(SPI_CHECK);
        if ((SPI_DATA_READY_MIN <= status) && (status <= SPI_DATA_READY_MAX)) {
            n = (SPI_DATA_READY_MIN == status) ? IQRF_NMAX : (status - SPI_DATA_READY_MIN);
            r = spi_transfer_data(n, buf_spi_out, READ_FROM);
            if (r > 0) {
                zmq_send(tx, buf_spi_out, n, 0);
            }
        } else if (COMMUNICATION_MODE == status) {
            n = zmq_recv(rx, buf_spi_in, IQRF_NMAX, ZMQ_DONTWAIT);
            if ((n > 0) && (n <= IQRF_NMAX)) {
                r = spi_transfer_data(n, buf_spi_in, WRITE_TO);
            }
        }
        bcm2835_delay(5);
    }

    if (sw_pwm) {
        printf("pthread_join()\n");
        if (pthread_join(pwm_thread, NULL)) {
            printf("pthread_join() error\n");
        }
    }

    //cleanup
    bcm2835_gpio_fsel(18, BCM2835_GPIO_FSEL_OUTP); //PWM pin
    bcm2835_gpio_clr(18);
    bcm2835_gpio_fsel(8, BCM2835_GPIO_FSEL_OUTP); /* CE0 */
    bcm2835_gpio_clr(8);
    bcm2835_gpio_fsel(9, BCM2835_GPIO_FSEL_OUTP); /* MISO */
    bcm2835_gpio_clr(9);
    bcm2835_gpio_fsel(10, BCM2835_GPIO_FSEL_OUTP); /* MOSI */
    bcm2835_gpio_clr(10);
    bcm2835_gpio_fsel(11, BCM2835_GPIO_FSEL_OUTP); /* CLK */
    bcm2835_gpio_clr(11);

    printf("bcm2835_close()\n");
    bcm2835_close();

    printf("zmq_close(spi_out_endpoint)\n");
    zmq_close(tx);
    printf("zmq_close(spi_in_endpoint)\n");
    zmq_close(rx);
    printf("zmq_term()\n");
    zmq_term(ctx);
    printf("%s terminated\n", binary_name);

    pthread_mutex_destroy(&bcm_mutex);

    return EXIT_SUCCESS;
}
